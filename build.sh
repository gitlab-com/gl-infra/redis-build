#!/bin/bash

set -efo pipefail

# renovate: datasource=github-releases depName=redis/redis
VERSION=7.0.15

if [[ -n $RELEASE_VERSION ]]; then
    VERSION="$RELEASE_VERSION"
fi

RELEASE="redis-$VERSION.$(uname -m)"

if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]; then
    if curl -I --silent --fail --header "JOB-TOKEN: $CI_JOB_TOKEN" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/redis/$VERSION/$RELEASE.tar.gz" >/dev/null 2>/dev/null; then
        echo "package for version $VERSION already exists"
        echo "see: https://gitlab.com/${CI_PROJECT_PATH}/-/packages"
        exit 0
    fi
fi

echo "Downloading..."
wget "https://github.com/redis/redis/archive/$VERSION.tar.gz" -O redis-$VERSION.tar.gz

tar xzf redis-$VERSION.tar.gz
cd redis-$VERSION

echo "Building..."
make CFLAGS="-fno-omit-frame-pointer"

mkdir -p .build/$RELEASE
mv src/{redis-benchmark,redis-cli,redis-server} .build/$RELEASE
cd .build

echo "Packaging..."
tar czfv $RELEASE.tar.gz $RELEASE

sha256sum $RELEASE.tar.gz

if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]; then
    echo "Uploading..."
    curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $RELEASE.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/redis/$VERSION/$RELEASE.tar.gz"
fi
