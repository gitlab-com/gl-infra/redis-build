# redis-build

This repository builds a Redis binary with debug symbols and frame pointers. See [gitlab-com/gl-infra/scalability!2020](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2020) for more context.

## Packages

Packaged binaries are available at: https://gitlab.com/gitlab-com/gl-infra/redis-build/-/packages/10944595.

## Updating the redis version

To produce a new build, open a merge request that bumps `VERSION` in `build.sh`.
